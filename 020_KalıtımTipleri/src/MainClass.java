
import single.Pantolon;
//import MultiLevelKalitim.*;
import MultiLevelKalitim.HyundaiI20;
import MultiLevelKalitim.Hyundai;
import MultiLevelKalitim.Araba;
import Hiyerasik.*;
public class MainClass {
    
    public static void main(String[] args)
    {
        Pantolon pnt = new Pantolon("kot pantolon", 80, 3, 0.3);
        pnt.fiyatHesapla();
        pnt.fisYaz();
        
        System.out.println("-----------------------");
        HyundaiI20 i20 = new HyundaiI20();
        i20.hiz(240);
        i20.marka();
        i20.model(2014);
        i20.renk("Beyaz");
        i20.yakitTipi("Dizel");
        System.out.println("-----------------------");
        
        Hyundai hyundai = new Hyundai();
        hyundai.marka();
        hyundai.model(2015);
        hyundai.renk("Kırmızı");
        hyundai.yakitTipi("Beyin");
        
        System.out.println("-----------------------");
        
        Araba araba = new Araba();
        araba.model(2018);
        araba.renk("Yeşil");
        araba.yakitTipi("Dizel");
        
        System.out.println("-----------------------");
        
        Doktor doktor = new Doktor();
        doktor.ameliyatEt();
        doktor.nefesAl();
        doktor.yemekYe();
        
        System.out.println("-----------------------");
         
        Muhendis muhendis = new Muhendis();
        muhendis.nefesAl();
        muhendis.yemekYe();
        muhendis.yazilimProjesiGelistir();
        
        System.out.println("-----------------------");
        
        Ogretmen ogretmen = new Ogretmen();
        ogretmen.dersVer();
        ogretmen.nefesAl();
        ogretmen.yemekYe();
        
        
        System.out.println("-----------------------");
        
        Tamirci tamirci = new Tamirci();
        tamirci.arabaTamiriYap();
        tamirci.nefesAl();
        tamirci.yemekYe();
    }
    
}
