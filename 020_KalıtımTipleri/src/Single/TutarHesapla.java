
package Single;

public class TutarHesapla {
    public String urunIsmi ; 
    public int urunAdet ;
    public int urunFiyat;
    public double kdvOran;
    public double tutar;

    public TutarHesapla(String urunIsmi, int urunAdet, int urunFiyat, double kdvOran, double tutar) {
        this.urunIsmi = urunIsmi;
        this.urunAdet = urunAdet;
        this.urunFiyat = urunFiyat;
        this.kdvOran = kdvOran;
        this.tutar = tutar;
    }
    
    public void fiyatHesabi()
    {
        tutar = Double.valueOf(urunFiyat * urunAdet ) + (urunFiyat * urunAdet *kdvOran);
    }
    
    
    public void fisYaz()
    {
        System.out.println(urunIsmi + " isimli üründen " + urunAdet + "tane alnmtr. Ödenecek Tutar = " + tutar);
    }
    
    
}
