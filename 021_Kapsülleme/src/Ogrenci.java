
public class Ogrenci {
    
    private String ogrenciAdi;
    private String ogrenciSoyadi ; 
    private int ogrenciNo;
    private int ogrenciSinif;
    
    public Ogrenci(String ogrenciAdi,String ogrenciSoyadi,int ogrenciNo )
    {
        this.ogrenciAdi = ogrenciAdi;
        this.ogrenciSoyadi = ogrenciSoyadi;
        this.ogrenciNo = ogrenciNo;
    }

    public String getOgrenciAdi() {
        return ogrenciAdi;
    }

 

    public String getOgrenciSoyadi() {
        return ogrenciSoyadi;
    }

    public int getOgrenciNo()
    {
        return ogrenciNo;
    }
    
    public void setOgrenciNo(int ogrenciNo)
    {
        this.ogrenciNo = ogrenciNo;
    }

    public int getOgrenciSinif() {
        return ogrenciSinif;
    }

    public void setOgrenciSinif(int ogrenciSinif) {
        this.ogrenciSinif = ogrenciSinif;
    }
    
    
}
