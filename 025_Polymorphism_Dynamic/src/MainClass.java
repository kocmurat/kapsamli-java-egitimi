
public class MainClass {
    
    
    public static void main(String[] args)
    {
        
        CalisanInsan insan = new CalisanInsan();
        insan.maasHesabi();
        insan.vergiOde();
        
        
        
        System.out.println("---------------------------------");
        
        insan = new Muhendis(15000);
        insan.vergiOde();
        insan.maasHesabi();
        insan.belgeNo();
        System.out.println("---------------------------------");
        
        insan = new Ogretmen(7000);
        insan.vergiOde();
        insan.maasHesabi();
        
       
    }
}
