
package DiziKlonlama;

import java.util.Arrays;
import java.util.Random;

public class DiziKlonlama {
    
    
    public static void main(String[] args)
    {
        int []sayilar = new int[100];
        for(int i=0;i<sayilar.length ; i++)
        {
            Random rnd = new Random();
            int sayi = rnd.nextInt(1000)+100;
            sayilar[i] = sayi;
        }
        String diziText = Arrays.toString(sayilar);
        System.out.println(diziText);
        int []cloneDizi = sayilar.clone();
        diziText = Arrays.toString(cloneDizi);
        System.out.println(diziText);
        
        System.out.println(sayilar[5]==cloneDizi[5]);
        System.out.println(sayilar[12]==cloneDizi[12]);
        System.out.println(sayilar[7]==cloneDizi[7]);
        System.out.println(sayilar[71]==cloneDizi[71]);
        System.out.println(sayilar[45]==cloneDizi[45]);
        
        System.out.println(sayilar==cloneDizi);
    }
}
