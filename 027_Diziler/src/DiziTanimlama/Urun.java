
package DiziTanimlama;


public class Urun {
    
    public String urunIsmi;
    public int urunFiyat;
    public String kategori;
    public String saticiFirma;

    public Urun(String urunIsmi, int urunFiyat, String kategori, String saticiFirma) {
        this.urunIsmi = urunIsmi;
        this.urunFiyat = urunFiyat;
        this.kategori = kategori;
        this.saticiFirma = saticiFirma;
    }

    @Override
    public String toString() {
        return "Urun{" + "urunIsmi=" + urunIsmi + ", urunFiyat=" + urunFiyat + ", kategori=" + kategori + ", saticiFirma=" + saticiFirma + '}';
    }
    
    
    
}
