
package Pdf2_DiziUzerindeIslemler;

import DiziTanimlama.Urun;

public class SinifTipindeDiziyeElemanEkleme {
    
    public static void main(String[] args)
    {
       Urun urunler[] = new Urun[3]; 
       Urun urun1 = new Urun("pantolon", 100, "giyim", "a firması");
       Urun urun2 = new Urun("gomlek", 70, "giyim", "c firması");
       Urun urun3 = new Urun("kazak", 49, "giyim", "g firması");
       
       urunler[0] = urun1;
       urunler[1] = urun2;
       urunler[2] = urun3;
       
       for(int i = 0 ; i<urunler.length ; i++)
       {
           System.out.println(urunler[i].toString());
       }
       
       
    }
}
