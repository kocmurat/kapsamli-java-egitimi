package ornekler;

public class Soru5 {

    public static void main(String[] args) {
        int[][] futbolcular = new int[5][3];

        // murat için
        futbolcular[0][0] = 14;
        futbolcular[0][1] = 21;
        futbolcular[0][2] = 8;

        //başar için
        futbolcular[1][0] = 5;
        futbolcular[1][1] = 26;
        futbolcular[1][2] = 21;

        // ege için 
        futbolcular[2][0] = 20;
        futbolcular[2][1] = 15;
        futbolcular[2][2] = 17;

        //erdi için
        futbolcular[3][0] = 4;
        futbolcular[3][1] = 9;
        futbolcular[3][2] = 11;

        // tamer için
        futbolcular[4][0] = 6;
        futbolcular[4][1] = 31;
        futbolcular[4][2] = 25;
        Soru5 nesne = new Soru5();
        nesne.toplamGol(futbolcular);
        nesne.toplamGol2017(futbolcular);
        nesne.enCokGol2018(futbolcular);
        nesne.egeGolSayisi(futbolcular);

    }

    public void toplamGol(int[][] dizi) {
        int toplam = 0;
        for (int i = 0; i < 5; i++) {
            for (int k = 0; k < 3; k++) {
                toplam = toplam + dizi[i][k];
            }
        }
        System.out.println("Toplam Gol Sayısı = " + toplam);
    }

    public void toplamGol2017(int[][] dizi) {
        int toplam = 0;
        for (int i = 0; i < 5; i++) {
            for (int k = 0; k < 1; k++) {
                toplam = toplam + dizi[i][k];
            }
        }
        System.out.println("2017 Yılında Toplam Gol Sayısı = " + toplam);
    }

    public void enCokGol2018(int[][] dizi) {
        int encokGol = 0;
        int indis = 0;
        for (int i = 0; i < 5; i++) {
            for (int k = 1; k < 2; k++) {
                if (dizi[i][k] > encokGol) {
                    encokGol = dizi[i][k];
                    indis = i;
                }
            }

        }
        if (indis == 0) {
            System.out.println("En Çok Golü Atan Murat , Gol Sayısı = " + encokGol);
        } else if (indis == 1) {
            System.out.println("En Çok Golü Atan Başar , Gol Sayısı = " + encokGol);
        } else if (indis == 2) {
            System.out.println("En Çok Golü Atan Ege , Gol Sayısı = " + encokGol);
        } else if (indis == 3) {
            System.out.println("En Çok Golü Atan Erdi , Gol Sayısı = " + encokGol);
        } else if (indis == 4) {
            System.out.println("En Çok Golü Atan Tamer , Gol Sayısı = " + encokGol);
        }
    }
    
    public void egeGolSayisi(int[][] dizi)
    {
        int toplam = 0 ; 
         for (int i = 2; i < 3; i++) {
            for (int k = 0; k <3; k++) {
                toplam = toplam + dizi[i][k];
            }

        }
         
         System.out.println("Egenin Toplam Gol Sayısı  = "+toplam);
    }
}
