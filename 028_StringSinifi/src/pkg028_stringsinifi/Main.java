/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg028_stringsinifi;

/**
 *
 * @author mk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String metin = "Türkiye Cumhuriyeti";
        System.out.println(metin);
        
        
        
        int karakterSayisi = metin.length();
        System.out.println(karakterSayisi);
        
        char charDeger = metin.charAt(5);
        System.out.println(charDeger);
        
        
        String subString1 = metin.substring(5);
        System.out.println(subString1);
        
        String subString2 = metin.substring(5, 10);
        System.out.println(subString2);
        
        String metin2 = metin.concat(" Devleti");
        System.out.println(metin2);
        
        
        int lastIndexOf = metin2.lastIndexOf("i");
        System.out.println(lastIndexOf);
       
        String metin3 = " Türkiye Cumhuriyeti Devleti ";
        System.out.println(metin3);
        System.out.println(metin2.length()+"--"+metin3.length());
        Boolean sonuc = metin2.equals(metin3);
        System.out.println(sonuc);
        
        metin3 = metin3.trim();
        sonuc = metin2.equals(metin3);
        System.out.println(metin2.length()+"--"+metin3.length());
        System.out.println(sonuc);
        
        String harf = "a";
        String harf2 = "a";
        
        int compareTo1 = harf.compareTo(harf2);
        System.out.println(compareTo1);
        
        harf2 = "b";
        int compareTo2 = harf.compareTo(harf2);
        System.out.println(compareTo2);
        
        harf = "c";
        int compareTo3 = harf.compareTo(harf2);
        System.out.println(compareTo3);
        
        
        
        String lower = metin2.toLowerCase();
        System.out.println(lower);
        
        
        String upper = metin2.toUpperCase();
        System.out.println(upper);
        
        String replace = metin2.replace("y", "a");
        System.out.println(replace);
        
        metin2 = "";
        Boolean empty = metin2.isEmpty();
        System.out.println(empty);
        
        String mailList = "kocmurat93@gmail.com,aaaa@gmail.com";
        String[] mailListArray = mailList.split(",");
        
        System.out.println(mailListArray[1]);
    }
    
}
