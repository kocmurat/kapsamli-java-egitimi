/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg031_list_interface;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       List<String> aile = new ArrayList<>();
       aile.add("murat");
       System.out.println(""+aile);
       
       
       Ogrenci ogrenci1 = new Ogrenci("murat", 21);
       Ogrenci ogrenci2 = new Ogrenci("basar", 23);
       List<Ogrenci> ogrenciler = new ArrayList<>();
       ogrenciler.add(ogrenci1);
       ogrenciler.add(ogrenci2);
       
       System.out.println(""+ogrenciler.toString());
    }
    
}
