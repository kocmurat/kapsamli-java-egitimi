/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg032_arraylist.ArraylistJavaSinifi;

/**
 *
 * @author mk
 */
public class Kisi {
    
    private String isim;
    private String meslek;
    private int yas;
    private int maas;

    public Kisi(String isim, String meslek, int yas, int maas) {
        this.isim = isim;
        this.meslek = meslek;
        this.yas = yas;
        this.maas = maas;
    }

    public String getIsim() {
        return isim;
    }

    public String getMeslek() {
        return meslek;
    }

    public int getYas() {
        return yas;
    }

    public int getMaas() {
        return maas;
    }

    
    
    @Override
    public String toString() {
        return "Kisi{" + "isim=" + isim + ", meslek=" + meslek + ", yas=" + yas + ", maas=" + maas + '}';
    }
    
    
    
}
