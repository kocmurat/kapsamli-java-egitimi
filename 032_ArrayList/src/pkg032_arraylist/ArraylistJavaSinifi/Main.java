/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg032_arraylist.ArraylistJavaSinifi;

import java.util.ArrayList;

/**
 *
 * @author mk
 */
public class Main {
    
    // murat yazılım mühendisi 27 50000
    public static void main(String[] args)
    {
        Kisi kisi1 = new Kisi("murat","yazılım mühendisi",27,50000);
        Kisi kisi2 = new Kisi("sezer","yusek insaat mühendisi",26,60000);
        Kisi kisi3 = new Kisi("ufuk","demirci",25,100000);
        Kisi kisi4 = new Kisi("hüseyin","öğretmen",24,55000);
        Kisi kisi5 = new Kisi("serkan","bilgisayar mühendisi",40,150000);
        
        ArrayList<Kisi> kisiler = new ArrayList<>();
        kisiler.add(kisi1);
        kisiler.add(kisi2);
        kisiler.add(kisi3);
        kisiler.add(kisi4);
        kisiler.add(kisi5);
       
        System.out.println(kisiler.toString());
        
        System.out.println("----------------------------------------");
        
        for(int i =0;i<kisiler.size();i++)
        {
            System.out.println(kisiler.get(i));
        }
        
        System.out.println("----------------------------------------");
        
        for(int i =0;i<kisiler.size();i++)
        {
            System.out.println(kisiler.get(i).getIsim());
        }
        
        System.out.println("----------------------------------------");
        
        for(int i =0;i<kisiler.size();i++)
        {
            System.out.println(kisiler.get(i).getMeslek());
        }
        
        
    }
}
