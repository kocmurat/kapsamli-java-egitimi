/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg032_arraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 *
 * @author mk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList<String> isimler = new ArrayList<>();
        System.out.println("--------------AddMetodu--------------------------");
        isimler.add("murat");
        isimler.add("basar");
        isimler.add("ege");
        isimler.add("tamer");
        isimler.add("erdi");

        System.out.println("" + isimler);
        System.out.println("Boyut : " + isimler.size());
        ArrayList<String> isimler2 = new ArrayList<>();
        isimler2.add("Rabia");
        isimler2.add("Güllü");

        System.out.println("-----------------AddAllMetodu-----------------------");
        isimler.addAll(isimler2);

        System.out.println("" + isimler);

        System.out.println("-----------------SizeMetodu-----------------------");
        System.out.println("Boyut : " + isimler.size());

        System.out.println("--------------------GetMetodu--------------------");
        String eleman = isimler.get(5);
        System.out.println("6 eleman = " + eleman);

        System.out.println("--------------------IteratorMetodu--------------------");
        Iterator<String> iter = isimler.iterator();

        System.out.println("--------------------IteratorMetodu-While--------------------");
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        System.out.println("--------------------IteratorMetodu-For--------------------");
        for (int i = 0; i < isimler.size(); i++) {
            System.out.println(isimler.get(i));
        }

        System.out.println("--------------------Integer-ArrayList--------------------");
        ArrayList<Integer> sayilar = new ArrayList<>();
        sayilar.add(5);
        sayilar.add(10);

        for (int i = 0; i < sayilar.size(); i++) {
            int deger = sayilar.get(i) * 5;
            System.out.println(deger);
        }
        Iterator<Integer> iter2 = sayilar.iterator();
        while (iter2.hasNext()) {
            System.out.println("" + iter2.next() * 5);
        }
        System.out.println("" + sayilar);
        System.out.println("--------------------Set Metodu--------------------");
        isimler.set(5, "Ibrahim");
        System.out.println("" + isimler);

        System.out.println("--------------------Set Metodu-Örnek--------------------");
        for (int i = 0; i < sayilar.size(); i++) {
            sayilar.set(i, sayilar.get(i) * 5);
        }
        System.out.println("" + sayilar);
        System.out.println("--------------------removeMetodu--------------------");
        isimler.remove(2);
        System.out.println("" + isimler);
        System.out.println("--------------------removeAllMetodu--------------------");
        isimler.removeAll(isimler);
        System.out.println("" + isimler);
        System.out.println("--------------------clearMetodu--------------------");
        sayilar.clear();
        System.out.println("" + sayilar);

        System.out.println("--------------------Döngüler--------------------");
        ArrayList<String> ulkeler = new ArrayList<>();
        ulkeler.add("Türkiye");
        ulkeler.add("Azerbaycan");
        ulkeler.add("Türkmenistan");
        System.out.println("--------------------Döngüler--While--------------------");

        Iterator<String> iter3 = ulkeler.iterator();
        while (iter3.hasNext()) {
            System.out.println(iter3.next());
        }

        System.out.println("--------------------Döngüler--For--------------------");
        for (int i = 0; i < ulkeler.size(); i++) {
            System.out.println(ulkeler.get(i));
        }
        System.out.println("--------------------Döngüler--Foreach--------------------");

        for (String eleman2 : ulkeler) {
            System.out.println(eleman2);
        }

        System.out.println("--------------------ArrayList-Array--------------------");
        ArrayList<Integer> ogrenciNumaraArrayList = new ArrayList<>();
        ogrenciNumaraArrayList.add(5);
        ogrenciNumaraArrayList.add(7);
        ogrenciNumaraArrayList.add(1);
        ogrenciNumaraArrayList.add(21);

        Object[] ogrenciNumaraArray = ogrenciNumaraArrayList.toArray();

        for (int i = 0; i < ogrenciNumaraArray.length; i++) {
            System.out.println("" + ogrenciNumaraArray[i]);
        }

        System.out.println("----------------------------------------");
        
        Integer[] ogrenciNumaraArray2 = 
                ogrenciNumaraArrayList.toArray(new Integer[ogrenciNumaraArrayList.size()]);
        
         for (int i = 0; i < ogrenciNumaraArray2.length; i++) {
            System.out.println("" + ogrenciNumaraArray[i]);
        }
        
        System.out.println("---------------------Array---ArrayList--------------------");
        ArrayList<Integer> ogrenciNumaraArrayList2 = new ArrayList<>(Arrays.asList(ogrenciNumaraArray2));
        
        for(int i = 0 ; i<ogrenciNumaraArrayList2.size();i++)
        {
            System.out.println(""+ogrenciNumaraArrayList2.get(i));
        }
        
        System.out.println("---------------------ArrayList---String--------------------");
        
        String stringArrayList = ogrenciNumaraArrayList2.toString();
        
         System.out.println(stringArrayList);
    }
    
    

}
