/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg034_hashsetsinifi;

import java.util.HashSet;

/**
 *
 * @author mk
 */
public class AramaveBoyutAlma {
    public static void main(String[] args)
    {
        HashSet<Integer> sayilar = new HashSet<>();
        sayilar.add(5);
        sayilar.add(25);
        sayilar.add(50);
        sayilar.add(78);
        sayilar.add(95);
        sayilar.add(12);
        
        boolean kontrol = sayilar.contains(21);
        System.out.println(kontrol);
        
        HashSet<String> isimler = new HashSet<>();
        isimler.add("Rabia");
        isimler.add("Ege");
        isimler.add("murat");
        isimler.add("Rabia");
        isimler.add("Ege");
        isimler.add("murat");
        isimler.add("Rabia");
        isimler.add("Ege");
        isimler.add("murat");
        
        
        int sayilarSize = sayilar.size();
        int isimlerSize = isimler.size();
        System.out.println(isimler.size());
        if(sayilarSize>isimlerSize)
        {
            System.out.println("Sayilar HashSet Eleman Sayisi İsimler "
                    + "HashSet Eleman Sayisindan büyüktür.  ");
        }else if ( isimlerSize > sayilarSize)
        {
             System.out.println("İsimler HashSet Eleman Sayisi Sayılar "
                    + "HashSet Eleman Sayisindan büyüktür.  ");
        }else
        {
            System.out.println("Eşittir");
        }
        
        
    }
}
