/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg034_hashsetsinifi;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author mk
 */
public class ElemanEkleme {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HashSet<String> kisiler = new HashSet<>();
        kisiler.add("murat");
        kisiler.add("başar");
        kisiler.add("ege");
        kisiler.add("tamer");
        kisiler.add("erdi");
        kisiler.add("bugra");
        kisiler.add("erdi");
        kisiler.add("murat");
        
        System.out.println(kisiler);
        
        ArrayList<String> kisiList = new ArrayList<>();
        kisiList.add("murat");
        kisiList.add("murat");
        System.out.println(kisiList);
        
        
        HashSet<Integer> sayilar = new HashSet<>();
        sayilar.add(5);
        sayilar.add(10);
        sayilar.add(5);
        sayilar.add(25);
        System.out.println(sayilar);
        
        
    }
    
}
