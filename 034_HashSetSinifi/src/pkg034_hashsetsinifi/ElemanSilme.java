/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg034_hashsetsinifi;

import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author mk
 */
public class ElemanSilme {
    
    public static void main(String[] args)
    {
        HashSet<String> ulkeler = new HashSet<>();
        ulkeler.add("Türkiye");
        ulkeler.add("İngiltere");
        ulkeler.add("İtalya");
        ulkeler.add("Türkmenistan");
        ulkeler.add("Azerbaycan");
        
        System.out.println(ulkeler);
        ulkeler.remove("İtalya");
        System.out.println(ulkeler);
        
        HashSet<String> silinecekUlkeler = new HashSet<>();
        silinecekUlkeler.add("Abd");
        silinecekUlkeler.add("İngiltere");
        silinecekUlkeler.add("Türkmenistan");
        
        ulkeler.removeAll(silinecekUlkeler);
        System.out.println(ulkeler);
        
        ulkeler.add("Rusya");
        ulkeler.add("Abd");
        ulkeler.add("Yunanistan");
        System.out.println(ulkeler);
        ArrayList<String> silinecekUlkelerList = new ArrayList<>();
        silinecekUlkelerList.add("Rusya");
        silinecekUlkelerList.add("Abd");
        
        ulkeler.removeAll(silinecekUlkelerList);
        System.out.println(ulkeler);
        
        ulkeler.clear();
        silinecekUlkeler.clear();
        silinecekUlkelerList.clear();
        
        System.out.println("ulkeler = "+ulkeler);
        System.out.println("silinecekUlkeler = "+silinecekUlkeler);
        System.out.println("silinecekUlkelerList ="+silinecekUlkelerList);
        
       
        
    }
}
