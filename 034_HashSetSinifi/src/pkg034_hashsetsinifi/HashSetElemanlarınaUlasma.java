/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg034_hashsetsinifi;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author mk
 */
public class HashSetElemanlarınaUlasma {
    public static void main(String[] args)
    {
        HashSet<String> hayvanlar = new HashSet<>();
        hayvanlar.add("kopek");
        hayvanlar.add("kedi");
        hayvanlar.add("kaplumbaga");
        hayvanlar.add("aslan");
        hayvanlar.add("balık");
        
        System.out.println("---------------------Iterator - While İle ------------");
        Iterator<String> iter = hayvanlar.iterator();
        
        while(iter.hasNext())
        {
            System.out.println(iter.next());
        }
        
        System.out.println("---------------------Foreach Yardımı ------------");
        
        for(String hayvan : hayvanlar)
        {
            System.out.println(hayvan); 
        }
        
        System.out.println("--------------------Java 8 Foreach Metodu ------------");
        
        hayvanlar.forEach(System.out::println);
        System.out.println("-------------------- ------------");
        ArrayList<String> hayvanList = new ArrayList<>();
        hayvanList.add("kopek");
        hayvanList.add("kedi");
        hayvanList.add("kaplumbaga");
        hayvanList.add("aslan");
        hayvanList.add("balık");
        hayvanList.add("zürafa");
        
        hayvanList.forEach(System.out::println);
        
    }
}
