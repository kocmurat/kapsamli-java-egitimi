/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg035_treeset;

import java.util.HashSet;
import java.util.TreeSet;

/**
 *
 * @author mk
 */
public class ElemanSilm {
    
    public static void main(String[] args)
    {
        
        TreeSet<String> isimler = new TreeSet<>();
        isimler.add("murat");
        isimler.add("hasan");
        isimler.add("süleyman");
        isimler.add("ali");
        isimler.add("yavuz");
        
        System.out.println(isimler);
        isimler.remove("süleyman");
        System.out.println(isimler);
        
        HashSet<String> isimler2 = new HashSet<>();
        isimler2.add("hasan");
        isimler2.add("murat");
        
        isimler.removeAll(isimler2);
        System.out.println(isimler);
        
        isimler.clear();
        System.out.println(isimler);
    }
}
