/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg035_treeset;

import java.util.Iterator;
import java.util.TreeSet;


public class ElemanlaraErisme {
    
    public static void main(String[] args)
    {
        TreeSet<Integer> ogrenciNotu = new TreeSet<>();
        ogrenciNotu.add(5);
        ogrenciNotu.add(95);
        ogrenciNotu.add(60);
        ogrenciNotu.add(78);
        ogrenciNotu.add(100);
        
        System.out.println(ogrenciNotu);
        
        Iterator<Integer> iter = ogrenciNotu.iterator();
        
        while(iter.hasNext())
        {
            Integer not = iter.next()*5;
            System.out.println(not);
        }
        
        System.out.println("-------------------------------");
        for(Integer not2:ogrenciNotu)
        {
            not2 = not2*10;
            System.out.println(not2);
        }
        
        System.out.println("-------------------------------");
        
        ogrenciNotu.forEach(x->
        {System.out.println(x*2);});
    }
}
