/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg035_treeset;

import java.util.HashSet;
import java.util.TreeSet;

/**
 *
 * @author mk
 */
public class IlkElemanSonEleman {
    public static void main(String[] args)
    {
        TreeSet<Integer> sayilar = new TreeSet<>();
        sayilar.add(4);
        sayilar.add(21);
        sayilar.add(94);
        sayilar.add(211);
        sayilar.add(164);
        sayilar.add(1);
        sayilar.add(44);
        sayilar.add(18);
        sayilar.add(-50);
        sayilar.add(210);
        
        int ilkEleman = sayilar.first();
        int sonEleman = sayilar.last();
        
        System.out.println("ilkEleman  = "+ilkEleman);
        System.out.println("sonEleman  = "+sonEleman);
        
        HashSet<Integer> sayilar2 = new HashSet<>();
        sayilar2.add(4);
        sayilar2.add(21);
        sayilar2.add(94);
        sayilar2.add(211);
        sayilar2.add(164);
        sayilar2.add(1);
        sayilar2.add(44);
        sayilar2.add(18);
        sayilar2.add(-50);
        sayilar2.add(210);
        
        //hashset sınıfı ile first ve last metodlarını kullanamayız. !!!
        //ilkEleman = sayilar2.first();
        //sonEleman = sayilar2.last();
        
        System.out.println("ilkEleman  = "+ilkEleman);
        System.out.println("sonEleman  = "+sonEleman);
        
        
        
    }
}
