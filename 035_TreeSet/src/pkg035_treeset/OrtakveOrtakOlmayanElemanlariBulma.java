/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg035_treeset;

import java.util.HashSet;
import java.util.TreeSet;

/**
 *
 * @author mk
 */
public class OrtakveOrtakOlmayanElemanlariBulma {
    
    public static void main(String[] args)
    {
        TreeSet<String> ulkeler = new TreeSet<>();
        ulkeler.add("Türkiye");
        ulkeler.add("Azerbaycan");
        ulkeler.add("Yunanistan");
        ulkeler.add("Rusya");
        
        System.out.println(ulkeler);
        
        TreeSet<String> ulkeler2 = new TreeSet<>();
        ulkeler2.add("Türkiye");
        ulkeler2.add("Azerbaycan");
        
        System.out.println(ulkeler2);
        
        System.out.println("-----------Ortak Elemanlar---------");
        
        //ulkeler.retainAll(ulkeler2);
        
        System.out.println(ulkeler);
        
        System.out.println("-----------Ortak Olmayan  Elemanlar---------");
        
        ulkeler.removeAll(ulkeler2);
        
        System.out.println(ulkeler);
        
        
        HashSet<String> ulkeler3 = new HashSet<>();
        ulkeler3.add("Türkiye");
        ulkeler3.add("Azerbaycan");
        ulkeler3.add("Yunanistan");
        ulkeler3.add("Rusya");
        
      
        
    }
    
}
