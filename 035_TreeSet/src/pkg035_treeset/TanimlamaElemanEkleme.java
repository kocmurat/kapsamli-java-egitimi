/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg035_treeset;

import java.util.HashSet;
import java.util.TreeSet;

/**
 *
 * @author mk
 */
public class TanimlamaElemanEkleme {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        TreeSet<Integer> sayilar = new TreeSet<>();
        sayilar.add(5);
        sayilar.add(50);
        sayilar.add(1);
        sayilar.add(-8);
        sayilar.add(21);
        
        System.out.println(sayilar);
        
        
        HashSet<Integer> sayilarHash = new HashSet<>();
        sayilarHash.add(-50);
        sayilarHash.add(21);
        sayilarHash.add(81);
        
        sayilar.addAll(sayilarHash);
        
        System.out.println(sayilar);
    }
    
}
