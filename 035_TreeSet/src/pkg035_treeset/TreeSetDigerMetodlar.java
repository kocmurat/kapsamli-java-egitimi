/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg035_treeset;

import java.util.TreeSet;

/**
 *
 * @author mk
 */
public class TreeSetDigerMetodlar {
    
    public static void main(String[] args)
    {
        TreeSet<Integer> sayilar = new TreeSet<>();
        sayilar.add(5);
        sayilar.add(50);
        sayilar.add(1);
        sayilar.add(-8);
        sayilar.add(21);
        sayilar.add(21);
        
        
        System.out.println(sayilar);
        //clone
        TreeSet<Integer> sayilar2 = (TreeSet<Integer>)sayilar.clone();
        System.out.println(sayilar2);
        
        int elemanSayisi = sayilar.size();
        System.out.println(elemanSayisi);
        
        boolean varmiyokmu = sayilar.contains(12);
        System.out.println(varmiyokmu);
        
        boolean bosmu = sayilar.isEmpty();
        
        System.out.println(bosmu);
        
        sayilar.clear();
        
        bosmu = sayilar.isEmpty();
        
        System.out.println(bosmu);
        
        
    }
}
