/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg036_linkedhashset;

import java.util.LinkedHashSet;



/**
 *
 * @author mk
 */
public class Birlestirme {
    public static void main(String[] args)
    {
     LinkedHashSet<Integer> sayilar = new LinkedHashSet<>();
     sayilar.add(1);
     sayilar.add(2);
     sayilar.add(3);
     
     System.out.println("------Birinci LinkedHashSet---------");
     System.out.println(sayilar.toString());
     
     LinkedHashSet<Integer> sayilar2 = new LinkedHashSet<>();
     sayilar2.add(11);
     sayilar2.add(22);
     sayilar2.add(33);
     
     System.out.println("------İkinci LinkedHashSet---------");
     System.out.println(sayilar2.toString());
     
     sayilar.addAll(sayilar2);
     
     System.out.println("------Birleştirme Sorunu---------");
     System.out.println(sayilar.toString());
     
     
     System.out.println("------Contains---------");
     
     System.out.println(sayilar.contains(1));
     
     
     System.out.println("------isEmpty---------");
     sayilar.clear();
     System.out.println(sayilar.isEmpty());
     
     LinkedHashSet<Integer> sayilar3 = (LinkedHashSet<Integer>)sayilar2.clone();
        
     
     System.out.println("------clone---------");
     
     System.out.println(sayilar3.toString());
     
     
    }
}
