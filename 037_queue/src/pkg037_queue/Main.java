/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg037_queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author mk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Queue<String> kuyruk = new LinkedList<>();
        kuyruk.offer("Kapsamlı");
        kuyruk.offer("Java");
        kuyruk.offer("Eğitimi");
        kuyruk.offer("Murat");
        
        System.out.println(kuyruk.toString());
        
        String kuyrukİlkEleman = kuyruk.peek();
        System.out.println(kuyrukİlkEleman);
        
        System.out.println(kuyruk.toString());
        
        String silinenEleman = kuyruk.poll();
        System.out.println(silinenEleman);
        
        System.out.println(kuyruk.toString());
        
    }
    
}
