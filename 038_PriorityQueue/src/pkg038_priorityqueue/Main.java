/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg038_priorityqueue;

import java.util.Iterator;
import java.util.PriorityQueue;

/**
 *
 * @author mk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // Kuyruk oluşturma
        PriorityQueue<Integer> numaralar = new PriorityQueue<>();
        
        //Eleman Ekleme
        numaralar.add(5);
        numaralar.add(8);
        numaralar.offer(12);
        numaralar.add(1);
        System.out.println(numaralar.toString());
        
        
        // elemanlara erişim
        Iterator<Integer> iter = numaralar.iterator();
        
        while(iter.hasNext())
        {
            System.out.println(iter.next()*5);
        }
        System.out.println("---------------------------------------");
        System.out.println("İlk Eleman = " + numaralar.peek());
        
        System.out.println(numaralar.toString());
        
        //System.out.println("---------------------------------------");
        //System.out.println("İlk Eleman = " + numaralar.poll());
        
        System.out.println(numaralar.toString());
        System.out.println("---------------------------------------");
        // Eleman Silme
        numaralar.remove(8);
        
        System.out.println(numaralar.toString());
        int ilkEleman = numaralar.poll();
        System.out.println(numaralar.toString());
        System.out.println("---------------------------------------");
        
        iter = numaralar.iterator();
        
        while(iter.hasNext())
        {
            System.out.println(numaralar.poll());
            
        }
        
        System.out.println(numaralar.toString());
    }
    
}
