/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg039_arraydeque;

import java.util.ArrayDeque;
import java.util.Iterator;

/**
 *
 * @author mk
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // nesne oluşturma
        ArrayDeque<String> isimler = new ArrayDeque<>();
        //Eleman ekleme - add 
        isimler.add("murat");
        isimler.add("ufuk");
        System.out.println(isimler.toString());
        isimler.addFirst("ege");
        System.out.println(isimler.toString());
        isimler.addLast("ali");
        System.out.println(isimler.toString());
        
        //eleman ekleme - offer
        
        isimler.offerFirst("erdi");
        isimler.offerLast("basar");
        System.out.println(isimler.toString());
        
        // get metodu kullanımı
        
        String ilkIsim = isimler.getFirst();
        String sonIsim = isimler.getLast();
        System.out.println("İlk İsim = " + ilkIsim + " son isim = "+ sonIsim);
        System.out.println(isimler.toString());
        
        //peek metodu kullanımı
        
        String ilkIsimPeek = isimler.peekFirst();
        String sonIsimPeek = isimler.peekLast();
        System.out.println("İlk İsim = " + ilkIsimPeek + " son isim = "+ sonIsimPeek);
        System.out.println(isimler.toString());
        
        // remove kullanımı
        
        String removeFirst = isimler.removeFirst();
        String removeLast  = isimler.removeLast();
        
        System.out.println("removeFirst= " + removeFirst + " removeLast = "+ removeLast);
        System.out.println(isimler.toString());
        
        // poll kullanımı
        
        String pollFirst = isimler.pollFirst();
        String pollLast  = isimler.pollLast();
        System.out.println("pollFirst= " + pollFirst + " pollLast = "+ pollLast);
        System.out.println(isimler.toString());
        
        // Elemana erişim
        Iterator<String> iter = isimler.iterator();
        while(iter.hasNext())
        {
            System.out.println(iter.next());
        }
        isimler.add("ali");
        isimler.add("sezer");
        isimler.addFirst("Cenk");
        System.out.println(isimler.toString());
        Iterator<String> iter2 = isimler.descendingIterator();
        
        while(iter2.hasNext())
        {
            System.out.println(iter2.next());
        }
        
    }
    
}
