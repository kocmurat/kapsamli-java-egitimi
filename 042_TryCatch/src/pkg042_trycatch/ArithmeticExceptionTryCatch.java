/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg042_trycatch;

/**
 *
 * @author mk
 */
public class ArithmeticExceptionTryCatch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int sayi1 = 10 ;
        int sayi2 = 0 ; 
        
        try{
           int bolme = sayi1 / sayi2 ; 
           System.out.println(bolme); 
        }catch(ArithmeticException e)
        {
            System.out.println("Hata "+e.getMessage());
            
        }
        
        System.out.println("Program çalışıyor");
        
        
    }
    
}
