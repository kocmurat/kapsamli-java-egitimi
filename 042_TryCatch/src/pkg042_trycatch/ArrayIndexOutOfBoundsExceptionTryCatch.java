/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg042_trycatch;

/**
 *
 * @author mk
 */
public class ArrayIndexOutOfBoundsExceptionTryCatch {
    
    public static void main(String[] args)
    {
        
        int array[] = new int[10];
       try{
        array[3]=5;
        array[9]=1;
       // array[10]=4;
       }catch(ArrayIndexOutOfBoundsException e)
       {
           System.out.println(e);
       }finally
       {
          System.out.println("finally içerisindeki kodlar çalıştı");
       }
        System.out.println("diğer kodlar çalıştı");
        
        
    }
}
