/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg042_trycatch;

/**
 *
 * @author mk
 */
public class NullPointerExceptionTryCatch {
    
    public static void main(String[] args)
    {
        try{
            
            String isim = null;
            isim.length();
            
        }catch(NullPointerException e)
        {
            System.out.println(e.getMessage());
        }
        
        System.out.println("Program çalışıyor");
        
    }
}
