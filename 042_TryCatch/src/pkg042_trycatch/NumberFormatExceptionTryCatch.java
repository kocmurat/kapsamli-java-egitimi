/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg042_trycatch;

import java.util.Scanner;

/**
 *
 * @author mk
 */
public class NumberFormatExceptionTryCatch {
    
    
    public static void main(String[] args)
    {
        
        System.out.println("Bir sayı gir");
        
        Scanner scn = new Scanner(System.in);
        
        String sayi = scn.nextLine();
        
        try{
        
        int sayiInt = Integer.parseInt(sayi);
        
        }catch(NumberFormatException e)
        {
            System.out.println(e);
        }
        
    }
    
}
