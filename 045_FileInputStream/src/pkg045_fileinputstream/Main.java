/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg045_fileinputstream;

import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author yazilimprojedunyasi
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        try {
            /*
            File f = new File("./src/files/kapsamli_java_egitimi.txt");
            FileInputStream fileInputStream = new FileInputStream(f);
            */
            FileInputStream fileInputStream = new FileInputStream("./src/files/kapsamli_java_egitimi.txt");
            /*
            int i = fileInputStream.read();
            System.out.println((char)i);
            
            while(i!=-1)
            {
                System.out.print((char)i);
                i = fileInputStream.read();
            }
            */
            fileInputStream.skip(7);
            System.out.println(fileInputStream.available());
            byte[] data = new byte[85];
            
            fileInputStream.read(data);
            System.out.println(fileInputStream.available());
            int deger = fileInputStream.read();
            
            System.out.println(deger);
            for(int i : data)
            {
                System.out.print((char)i);
            }
            fileInputStream.close();
        } catch (Exception e) {
        
            e.printStackTrace();
        }
        
    }
    
}
