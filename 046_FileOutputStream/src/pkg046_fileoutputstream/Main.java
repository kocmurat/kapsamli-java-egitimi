/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg046_fileoutputstream;

import java.io.FileOutputStream;

/**
 *
 * @author yazilimprojedunyasi
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String veri = "Merhaba , Kapsamli Java Egitimine Hos Geldiniz...";
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("./src/files/output1.txt",false);
            
            fileOutputStream.write(109);
            byte[] array = veri.getBytes();
            fileOutputStream.write(array);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
