/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg047_filereader;

import java.io.FileReader;

/**
 *
 * @author yazilimprojedunyasi
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            
            FileReader fileReader = new FileReader("./src/files/output1.txt");
          
            /*
            int i = fileReader.read();
            
            while(i!=-1)
            {
                System.out.print((char)i);
                i = fileReader.read();
            }
           */
            char[] array = new char[100];
            fileReader.read(array);
            for(char chr : array)
            {
                System.out.print(chr);
            }
            fileReader.close();
            
            
            char[] array2 = new char[100];
            fileReader.read(array2);
            for(char chr : array2)
            {
                System.out.print(chr);
            }
        } catch (Exception e) {
            
            e.printStackTrace();
        
        }
        
    }
    
}
